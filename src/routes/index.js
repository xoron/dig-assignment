import React from 'react';
import { StackNavigator } from 'react-navigation';
import Product from './product/ProductContainer';
import Details from './details/DetailsContainer';

export const MainStack = StackNavigator(
    {
        Product: {
            screen: Product
        },
        Details: {
            screen: Details
        }
    },
    {
        initialRouteName: 'Product',
        headerMode: 'none'
    }
);

export default () => <MainStack />;
