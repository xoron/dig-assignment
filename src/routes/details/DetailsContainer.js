import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Details from './Details';
import {} from './detailsActions';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Details);
