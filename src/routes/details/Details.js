import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { Container, Content, Text, Header, Left, Body, Right, Title, H1, H2, H3, Button, Icon } from 'native-base';
import Gallery from './components/Gallery';
import Comments from './components/comments/CommentsContainer';

class Details extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { product } = this.props.navigation.state.params;

        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" /> <Text>Back</Text>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Details</Title>
                    </Body>
                    <Right />
                </Header>
                <Content padder>
                    <H1>{product.title}</H1>

                    <H2>Description:</H2>
                    <Text>{product.description}</Text>

                    <H2>Specification:</H2>
                    <Text>{product.specification}</Text>

                    <H3>Price: {product.price}</H3>

                    <Gallery images={product.images} />
                    <Comments id={product.id} comments={product.comments} />
                </Content>
            </Container>
        );
    }
}

export default Details;
