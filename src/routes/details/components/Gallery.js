import React, { Component } from 'react';
import { View, TouchableOpacity, Image } from 'react-native';


class Gallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedImage: props.images[0].original
        };
    }

    render() {
        const { images } = this.props;
        const { selectedImage } = this.state;

        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    {images.map((image, index) => (
                        <TouchableOpacity key={index} onPress={() => this.setState({ selectedImage: image.original })}>
                            <Image source={{ uri: image.thumb }} style={{ height: 100, width: 100 }} />
                        </TouchableOpacity>
                    ))}
                </View>

                <Image source={{ uri: selectedImage }} style={{ height: 300, width: 300 }} />
            </View>
        );
    }
}

export default Gallery;
