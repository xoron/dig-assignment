import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Comments from './Comments';
import { addComment } from '../../../product/productActions';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => bindActionCreators({
    addComment
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Comments);
