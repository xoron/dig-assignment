import React, { Component } from 'react';
import { View } from 'react-native';
import { List, ListItem, Body, Text, Form, Label, Item, Input, Button } from 'native-base';

class Comments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            comments: props.comments
        };
    }

    handleAddComment() {
        const {
            id,
            addComment
        } = this.props;
        const {
            text,
            comments
        } = this.state;

        addComment(id, text);
        this.setState({
            comments: [...comments, text],
            text: ''
        });
    }

    render() {
        const {
            comments,
            text
        } = this.state;

        return (
            <View style={{ flex: 1 }}>
                <List>
                    {!!comments && comments.map((comment, index) => (
                        <ListItem key={index}>
                            <Body>
                                <Text>{comment}</Text>
                            </Body>
                        </ListItem>
                    ))}
                </List>
                <Form>
                    <Item floatingLabel>
                        <Label>Add Comment</Label>
                        <Input value={text} onChangeText={newVal => this.setState({ text: newVal })} />
                    </Item>
                    <Button
                        onPress={() => this.handleAddComment()}
                    ><Text>Add Comment</Text></Button>
                </Form>
            </View>
        );
    }
}

export default Comments;
