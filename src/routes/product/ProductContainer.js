import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NavigationActions } from 'react-navigation';
import Product from './Product';
import { getProductList } from './productActions';

const mapStateToProps = ({
    navigation,
    route: {
        product: { productList }
    }
}) => ({
    navigation,
    productList
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getProductList,
    details: product => NavigationActions.navigate({ routeName: 'Details', params: { product } })
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Product);
