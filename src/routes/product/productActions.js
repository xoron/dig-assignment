export const FETCH_DATA = 'FETCH_DATA';
export const FETCH_DATA_REQUEST = 'FETCH_DATA_REQUEST';
export const FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS';
export const FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE';

export const ADD_COMMENT = 'ADD_COMMENT';

export const getProductList = () => ({
    type: FETCH_DATA,
    promise: fetch => fetch.get('http://private-5815fe-recommendationsknip.apiary-mock.com/products')
});

export const addComment = (id, comment) => ({
    type: ADD_COMMENT,
    payload: {
        id,
        comment
    }
});
