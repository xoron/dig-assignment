import {
    FETCH_DATA_SUCCESS,
    ADD_COMMENT
} from './productActions';

const initialState = {
    productList: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DATA_SUCCESS:
            return {
                ...state,
                productList: action.result.map(product => ({ ...product, comments: [] }))
            };
        case ADD_COMMENT:
            return {
                ...state,
                productList: state.productList.map(product => (
                    product.id === action.payload.id
                        ? { ...product, comments: [...product.comments, action.payload.comment] }
                        : product
                ))
            };
        default:
            return state;
    }
};
