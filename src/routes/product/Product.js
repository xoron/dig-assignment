import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { Container, Content, Text, Header, Left, Right, Body, Title, List, ListItem, Thumbnail, Button, Icon } from 'native-base';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        const { getProductList, productList } = this.props;
        if (!productList) getProductList();
    }

    render() {
        const { productList, details, getProductList } = this.props;

        return (
            <Container>
                <Header>
                    <Left />
                    <Body>
                        <Title>Products</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => getProductList()}>
                            <Text>reload</Text> <Icon name="md-refresh" />
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <List>
                        {!!productList && productList.map(product => (
                            <ListItem thumbnail key={product.id}>
                                <Left>
                                    <Thumbnail square source={{ uri: product.images[1].thumb }} />
                                </Left>
                                <Body>
                                    <Text>{product.title}</Text>
                                    <Text note numberOfLines={1}>{product.price}</Text>
                                </Body>
                                <Right>
                                    <Button transparent onPress={() => details(product)}>
                                        <Text>View</Text>
                                    </Button>
                                </Right>
                            </ListItem>
                        ))}
                    </List>
                </Content>
            </Container>
        );
    }
}

export default Product;
