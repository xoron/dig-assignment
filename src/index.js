import React, { Component } from 'react';
import Expo from 'expo';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

// Redux Store
import {
    store,
    persistor
} from './redux/store';

import AppWithNavigationState from './redux/navigation';

class Main extends Component {
    async componentDidMount() {
        await Expo.Font.loadAsync({
            Roboto: require('native-base/Fonts/Roboto.ttf'), // eslint-disable-line
            Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'), // eslint-disable-line
        });
    }

    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <AppWithNavigationState />
                </PersistGate>
            </Provider>
        );
    }
}

export default Main;
