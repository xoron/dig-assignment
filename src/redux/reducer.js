import { combineReducers } from 'redux';

import routeReducer from '../routes/routeReducer';
import { navigationReducer } from './navigation';

const reducer = combineReducers({
    route: routeReducer,
    navigation: navigationReducer
});

export default reducer;
