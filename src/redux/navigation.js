import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    addNavigationHelpers
} from 'react-navigation';
import {
    createReduxBoundAddListener,
    createReactNavigationReduxMiddleware
} from 'react-navigation-redux-helpers';
import { MainStack as AppNavigator } from '../routes/index';

const initialState = AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams('Product'));

export const navigationMiddleware = createReactNavigationReduxMiddleware(
    'Product',
    state => state.navigation,
);

const addListener = createReduxBoundAddListener('Product');

export const navigationReducer = (state = initialState, action) => {
    const nextState = AppNavigator.router.getStateForAction(action, state);

    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
};

const App = ({ dispatch, navigation }) => (
    <AppNavigator
        navigation={addNavigationHelpers({
            state: navigation,
            dispatch,
            addListener
        })}
    />
);

App.propTypes = {
    dispatch: PropTypes.func.isRequired,
    navigation: PropTypes.object.isRequired
};

const mapStateToProps = ({ navigation }) => ({ navigation });

const AppWithNavigationState = connect(mapStateToProps)(App);

export default AppWithNavigationState;
