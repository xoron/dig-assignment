import { createStore, applyMiddleware } from 'redux';

import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native

import { navigationMiddleware } from './navigation';
import customMiddleware from './customMiddleware';
import reducer from './reducer';

const persistConfig = {
    key: 'root',
    storage
};

const persistedReducer = persistReducer(persistConfig, reducer);

export const store = createStore(
    persistedReducer,
    applyMiddleware(
        logger,
        thunkMiddleware,
        customMiddleware,
        navigationMiddleware
    )
);
export const persistor = persistStore(store);
