import fetch from './clients/fetchClient';

export default ({ dispatch }) => next => (action) => {
    if (!action.promise) return next(action);

    const { promise, type, ...rest } = action;

    const REQUEST = `${type}_REQUEST`;
    const SUCCESS = `${type}_SUCCESS`;
    const FAILURE = `${type}_FAILURE`;

    dispatch({ ...rest, type: REQUEST });

    return promise(fetch)
        .then(result => dispatch({ ...rest, result, type: SUCCESS }))
        .catch(error => dispatch({ ...rest, error, type: FAILURE }));
};
